#! /usr/bin/env python3
# -*- coding: utf-8 -*-
# Author: AFCC Extended Development Team
# License: TBD
# Copyright (c) 2012, TBD
r"""
*************************************************************************
:mod:`pem_analytic`: Analytic Fuel Cell Models
*************************************************************************

.. module:: pem_analytic

Contents
--------
The PythonFCST package imports all the functions from the top level modules. 
 
Import
------
>>> import pem_analytic as pem

Submodules
----------
::

 None                            --- No subpackages at the moment


"""

from __future__ import print_function

import sys

import scipy as sp
import pandas as pd

from copy import copy, deepcopy

import pint
u = pint.UnitRegistry()


class Constants(object):
    r"""
    General container for unit laden constants.
    """
    
    def __init__(self):
        self._unitdict = {}
        self.to_base_units()

    def _update(self):
        return 1    
    
    def __getitem__(self,key):
        self._update()
        return self._unitdict[key]
        
    def to_base_units(self):
        r"""
        Converts unit dictionary to base SI units
        """
        
        for key in self._unitdict.keys():
            try:
                self._unitdict[key] = self._unitdict[key].to_base_units()
            except:
                #print( "Unexpected error:", sys.exc_info()[0] )
                pass
    
    def get_lmfit_params(self):
        r"""
        Returns the lmfit parameters class of all parameters
        """
        
        from lmfit import Parameters
        
        if hasattr(self,'_lmfit_params'):
            print('Retrieving lmfit parameters')
            return self._lmfit_params
        else:
            print('Creating LMFIT Object')
            params = Parameters()
            
            for key in self._unitdict.keys():
                print(key)
                try:
                    params.add(key,value=self._unitdict[key].magnitude,vary=False)
                except:
                    print('Exception in key ',key)
                    params.add(key,value=self._unitdict[key],vary=False)
                    pass
            self._lmfit_params = params
            return params
        
    def set_lmfit_params(self, params):
        r"""
        Add documentation
        """
        for key in params.keys():
            if key in self._unitdict.keys():
                try:
                    self._unitdict[key] = params[key].value * self._unitdict[key].units 
                except:
                    print('Exception in key ',key)
                    self._unitdict[key] = params[key]
                    pass
            else:
                print('Key not found in parameters: ', key)
        self._lmfit_params = params.copy()
        self.update()
    
    def print(self):
        r""" 
        Print all defined units
        """
        
        
        print('Values stored in ', self.__class__,":")
        for key in self._unitdict.keys():
            print(key,'\t = ', self._unitdict[key])
        print("\n")
        
    def copy(self):
        return copy(self)
    
    def update(self):
        r"""
        ABS for update function
        """
        return 1


class PhysicalConstants(Constants):
    r"""
    Class containing the physical constants relevant for the polcurves
    
    Add more documentation.
    """
    
    def __init__(self):
        super(PhysicalConstants,self).__init__()
        import scipy.constants as spc
                
        self._unitdict = {}
        self._unitdict['R'] = spc.R * u.J / u.mol / u.K
        self._unitdict['F'] = spc.physical_constants['Faraday constant'][0] * u.C / u.mol
        self._unitdict['air molar mass'] = 28.9645 * u.gram / u.mol        
        self.to_base_units()
        
        self.R = self._unitdict['R']
        self.F = self._unitdict['F']
        self.air_molar_mass = self._unitdict['air molar mass']

pconsts = PhysicalConstants()
        

class ORRReference(Constants):
    r"""
    Class containing the reference conditions for the ORR reaction
    """
    def __init__(self):
        super(ORRReference,self).__init__()
        
        self._unitdict['T0']  = 298.15 * u.K  # Reference Temperature
        self._unitdict['p_0'] = 1.01325 * u.bar # Reference Pressure
        self._unitdict['n']   = 2               # Number of electrons transferred
        self._unitdict['E0']  = 1.229 * u.V     # Ideal OCV
        self._unitdict['deltaS0'] = -163.11 * u.J / u.mol / u.K

        self.to_base_units()
        
        self.T0 = self._unitdict['T0']
        self.p_0 = self._unitdict['p_0']
        self.n  = self._unitdict['n']
        self.E0 = self._unitdict['E0']
        self.deltaS0 = self._unitdict['deltaS0']
        


class OperatingConditions(Constants):
    r"""
    Class to convert and store operating conditions.
    
    This class takes operating conditions in their native form and converts 
    them to SI.
    
    Parameters:
    -----------
    
    to be added
    
    Examples:
    ---------
    
    To be added
    """
    
    def __init__(self,
                 rhCathode=0.7, rhAnode=0.7,
                 pCathode=2.5*u.bar, pAnode=2.7*u.bar,
                 T_Cool=363.15 * u.K):
        print('Initialize Operating Conditions')
        
        super(OperatingConditions,self).__init__()
        
        self._unitdict['rH Cathode']  = rhCathode
        self._unitdict['rH Anode'] =rhAnode
        
        self._unitdict['P Anode'] = pAnode
        self._unitdict['P Cathode'] =  pCathode
        self._unitdict['T Cool'] = T_Cool
        self._unitdict['O2 in air'] = 0.20946
        self._unitdict['Reference O2 Concentration'] = 7.36e-6 * u.mol / u.cm**3
        self.to_base_units()
        
        self.p_a = self._unitdict['P Anode']
        self.p_c = self._unitdict['P Cathode']
        self.T = self._unitdict['T Cool']
        
        self._unitdict['O2 Concentration'] = self.calc_cO2()
        self.cO2 = self._unitdict['O2 Concentration']
    
    def calc_cO2(self):
        r"""
        Calculate O2 concentration at current pressure and Temperature"     
        """
        pconst = PhysicalConstants()
        return  self.p_c/(pconst.R*self.T)*self._unitdict['O2 in air']



class KulikovskyParameters(Constants):
    r"""
    Class to store the parameters for the Kulikoskuy model.
    
    This class takes values in their native form and converts 
    them to SI.
    
    Parameters:
    -----------
    
    to be added
    
    Examples:
    ---------
    
    To be added
    """
    
    def __init__(self,
                 sigma_p_CCM=0.03*u.siemens/u.cm,
                 r_ohmic=0.08*u.ohm*u.cm**2,
                 tafel_slope=0.03*u.V,
                 i_ex=0.817e-3*u.A/u.cm**3,
                 D_GDL=0.0259*u.cm**2/u.s,
                 D_CCL=1.36e-3*u.cm**2/u.s,
                 ActiveArea=45.15*u.cm**2,
                 GDL_Thickness=0.025*u.cm,
                 CCL_Thickness=0.025*u.cm,
                 PEM_Thickness=0.0025*u.cm):
                     
        super(KulikovskyParameters,self).__init__()
        # Effective Properties
        self._unitdict['CCL_Proton_Cond'] = sigma_p_CCM
        self._unitdict['Ohmic_Resistance'] = r_ohmic
        self._unitdict['Tafel_Slope'] = tafel_slope
        self._unitdict["Exchange_Current"] = i_ex
        self._unitdict['GDL_Diffusivity'] = D_GDL
        self._unitdict['CCL_Diffusivity'] = D_CCL
        # MEA dimensions
        self._unitdict['Area'] = ActiveArea
        self._unitdict['GDL_Thickness'] = GDL_Thickness
        self._unitdict['CCL_Thickness'] = CCL_Thickness
        self._unitdict['PEM_Thickness'] = PEM_Thickness
        self.to_base_units()
        self.update()
        
    def update(self):
        # Define Attributes
        self.to_base_units()
        self.sigma_p_CCM = self._unitdict['CCL_Proton_Cond']
        self.R_Ohm = self._unitdict['Ohmic_Resistance']
        self.tafel = self._unitdict['Tafel_Slope']
        self.i_ex = self._unitdict["Exchange_Current"]
        self.D_GDL = self._unitdict['GDL_Diffusivity']
        self.D_CCL = self._unitdict['CCL_Diffusivity']

      
class Kulikovsky(object):
    r"""
    This class implements the Kulikovsky model.
    
    Similar to the Srinivasan equation, the cell voltage is broken down in
    several voltage loss components. In this case
    
    .. math::

        V_\text{cell}(j) = V_\text{Nernst} - \eta_0(j) 
            - \eta_\text{trans,GCL}(j)
            - \eta_\text{trans,CCL}(j)
            - R_\Omega \ j
    
    
    
    Source: A.A.Kulikovsky, JES, 161 F263-F270, 2014
    
    Parameters:
    -----------
    OpsCon : OperatingConditions(), optional
        Operating conditions class. Initialized with default parameters. 
    Params : KulikovskyParameters(), optinal
        Parameter containers holding all Kulikovsky parameters. Initialized
        with default parameters if not specified.
    """
    
    def __init__(self,
                 OpsCon=OperatingConditions(),
                 Params=KulikovskyParameters()):
        print('='*50,"\nInitializing Kulikovsky Model")
        print('-'*50,'\n\n')
        self._OpsCon = OpsCon.copy()
        self._Params = Params.copy()
        self._ORR = ORRReference()
        self._Const = PhysicalConstants()
        print('-'*50)
        print('\t Operating Conditions')
        print('-'*50)
        OpsCon.print()
        print('-'*50)
        print('\t Kulikovsky Parameters')
        print('-'*50)
        Params.print()
        print('-'*50)
        print('\t Kulikovsky Initialized')
        print('='*50,'\n\n')
        
    
    def print_diagnostics(self):
        print('='*50,"\nKulikovsky Diagnostics")
        print('-'*50,'\n')
        
        print('-'*50)
        print('\t Internal Limiting Currents')
        print('-'*50)
        print('j^star    = ', self.j_star())
        print('j^starlim = ', self.j_starlim())
        print('j^sigma   = ', self.j_sigma())
        print('beta      = ',    self.beta())
        
        print('-'*50)
        print('\t Individual Voltage Contributions at 1 A/cm^2')
        print('-'*50)
        print('\tV_Nernst  = ', self.V_Nernst())
        print('\tV_Act_ORR = ', self.V_act_ORR())
        print('\tV_mtl_CCL = ', self.V_mtl_CCL())
        print('\tV_mtl_GDL = ', self.V_mtl_GDL())
        print('\tV_Ohm     = ', self.V_ohm())
        print('\tV_Cell    = ', self.V_cell())
        print('-'*50)
        print('\t Voltage Loss Breakdown at 1 A/cm^2')
        print('-'*50)
        vlb = self.VLB()
        for key in vlb.keys():
            print('\t',key,'\t= ',vlb[key])
        
        print('-'*50)
        print('\t Calculate minimum GDL diffusivity ')
        print('-'*50)
        print('\nMinium GDL diffusivity to achieve 3.5 A/cm**2 = ', self.calc_min_D_GDL())
        
        print('-'*50)
        print('\t Diagnostic Complete')
        print('='*50,'\n\n')
    
    def j_star(self):
        r"""
        Computes jstar, whatever this is
        """
        
        return self._Params.sigma_p_CCM*self._Params.tafel/self._Params['CCL_Thickness']
        
    def j_starlim(self):
        r"""
        Limiting current due to GDL diffusivity restrictions.
        
        .. math::
            j_\text{lim}^\star = \frac{4 F D_\text{GDL} c_{O_2}}{l_\text{GDL}}
        """
        
        return 4*self._Const.F*self._Params.D_GDL*self._OpsCon.cO2/self._Params['GDL_Thickness']
        
    def calc_min_D_GDL(self,ilim=3.5*u.A/u.cm**2):
        
        ilim=ilim.to_base_units()
        return ilim/(4*self._Const.F*self._OpsCon.cO2/self._Params['GDL_Thickness'])
        
    def j_sigma(self):
        r"""
        Computes the current loss die to reduced protonic potentail ???
        """
        
        return (2*self._Params.i_ex*self._Params.sigma_p_CCM*self._Params.tafel)**0.5
        
    def beta(self,j=1.0*u.A/u.cm**2):
        r"""
        Comptes beta, whatever beta is
        """
        
        j = j.to_base_units()
        j_star = self.j_star()        
        
        return sp.sqrt(2*j/j_star)/(1+sp.sqrt(1.12*j/j_star)*sp.exp(sp.sqrt(2*j/j_star)))+sp.pi*j/j_star/(2+j/j_star)        
        
    def V_Nernst(self):
        r"""
        Returns the Nernst potential of the ORR reaction
        
        More documentation !!!
        """
        
        V_Nernst = self._ORR.E0
        
        tmp1 = self._Const.R*self._ORR.T0/self._ORR.n/self._Const.F
        tmp2 = sp.sqrt(self._OpsCon.p_c*self._OpsCon._unitdict['O2 in air']/self._ORR.p_0)
        tmp3 = self._OpsCon.p_a/self._ORR.p_0
        V_Nernst = V_Nernst + tmp1*sp.log(tmp3*tmp2)
        
        tmp1 = self._ORR.deltaS0/self._ORR.n/self._Const.F
        tmp2 = self._OpsCon.T-self._ORR.T0
        V_Nernst = V_Nernst + tmp1*tmp2
        
        return V_Nernst.to(u.V)
        
    def V_act_ORR(self,j=1.0*u.A/u.cm**2):
        r"""
        Activation overpotential and proton transport losses assuming
        ideal oxygen transport
        
        .. math::
        
            \eta_0 = b \arcsin
            \left(
                \frac{\left( \frac{j}{j_\sigma} \right)^2}
                {2\frac{c_{O_2}}{c_\text{ref}} 
                \left( 1-\exp \left( \frac{j}{j_\star}\right)\right)                
                }
            \right)
        
        Parameters:
        -----------
        j : unit scalar/vector [A/cm**2]
            Current density in units equivalent to A/m**2. 
            Automatic conversion to SI base units.
        """
        j = j.to_base_units()        
        
        j_star  = self.j_star()
        j_sigma = self.j_sigma()
        tafel   = self._Params.tafel         
        
        tmp01 = self._OpsCon.cO2/self._OpsCon['Reference O2 Concentration']
        tmp = tafel*sp.arcsinh((j/j_sigma)**2/(2*tmp01*(1-sp.exp(-j/2/j_star))))
        return tmp.to(u.V) 
        
    def V_mtl_CCL(self,j=1.0*u.A/u.cm**2):
        r"""
        Activiation losses due to losses in the CCL
        """
        j = j.to_base_units()        
        
        j_star  = self.j_star()
        j_starlim = self.j_starlim()
        tafel   = self._Params.tafel
        
        fact01 = self._Params.sigma_p_CCM*tafel**2/(4*self._Const.F*self._Params.D_CCL*self._OpsCon.cO2)     
        fact02 = j/j_star-sp.log(1+j**2/(j_star**2*self.beta(j=j)**2))
        deno01 = self._OpsCon.cO2/self._OpsCon['Reference O2 Concentration']
        tmp = fact01*fact02/(1-j/j_starlim/deno01);

        return tmp.to(u.V) 
        
    def V_mtl_GDL(self,j=1.0*u.A/u.cm**2):
        r"""
        Activiation losses due to losses in the GDL
        """
        j = j.to_base_units()        
        
        j_starlim = self.j_starlim()
        tafel   = self._Params.tafel
        
        deno01 = self._OpsCon.cO2/self._OpsCon['Reference O2 Concentration']
        tmp = -tafel*sp.log(1-j/j_starlim/deno01);

        return tmp.to(u.V) 
        
    def V_ohm(self,j=1.0*u.A/u.cm**2):
        r"""
        Activiation losses due to losses in the GDL
        """
        
        j = j.to_base_units()        
        
        tmp = self._Params.R_Ohm*j

        return tmp.to(u.V) 
                         
    def V_cell(self,j=1.0*u.A/u.cm**2):
        r"""
        Activiation losses due to losses in the GDL
        """
        
        j = j.to_base_units()
        V_act = self.V_act_ORR(j) + self.V_mtl_CCL(j) + self.V_mtl_GDL(j)
        V_cell= self.V_Nernst() - V_act - self.V_ohm(j)
        
        return V_cell.to(u.V)
        
    def VLB(self,j=1.0*u.A/u.cm**2):
        r"""
        Voltage loss breakdown.
        """
        
        j = j.to_base_units()
        
        tmp = {}
        tmp['i [A/cm**2]'] = j.to(u.A/u.cm**2).magnitude
        tmp['V_Nernst [V]']  = self.V_Nernst().magnitude
        tmp['V_act_ORR [V]'] = self.V_act_ORR(j).magnitude
        tmp['V_mtl_CCL [V]'] = self.V_mtl_CCL(j).magnitude
        tmp['V_mtl_GDL [V]'] = self.V_mtl_GDL(j).magnitude
        tmp['V_ohm [V]']     = self.V_ohm(j).magnitude
        tmp['V_cell [V]']    = self.V_cell(j).magnitude
        
        return tmp
        
    def get_lmfit_params(self):
        return self._Params.get_lmfit_params()
        
    def set_lmfit_params(self,params):
        self._Params.set_lmfit_params(params)
        
    def residual(self, params,j , data = None, method='default',eps = None):
        r"""
        Returns the residual of the Kulikovsky bs given experimental data
        
        Parameters:
        -----------
        params : lmfit.parameters
            Lmfit parameter obkect with updated model parameters
        
        j : pint.Quantity
            Scalar or vector of current densities the proper unit.
        data : pint.Quantity
            Scalar/vector of measured voltages
        """
        
        if params != None:
            self.set_lmfit_params(params)
        if data != None:
            data = data.to_base_units().magnitude
        
        model_data = self.V_cell(j).magnitude
                        
        if sp.any(sp.iscomplex(model_data))==True:
            print('\tComplex Values Found')
            model_data = sp.absolute(model_data)+5
        if data is None:
            #print('\tNo Experimental Data, reurn model')
            residual =  model_data
            
            
        if method == 'default':
            #print("\t Return unweighted residual")
            residual = sp.absolute(model_data - data)
            #return sp.divide(sp.absolute(model_data - data), data)
            #return 1*sp.absolute(model_data - data) + sp.divide(sp.absolute(model_data - data), data)
        elif method == 'relative':
            residual = sp.divide(sp.absolute(model_data - data), data)
        elif method == 'mixed':
            residual =  sp.absolute(model_data - data) + sp.divide(sp.absolute(model_data - data), data)
        
        if eps is None:
            return residual
        else:
            return eps*residual
    
        
        

def main():
    print("Some small demo")
    
    pconst = PhysicalConstants()
    pconst.print()
    
    pORR  = ORRReference()
    pORR.print()
    
    pOpc = OperatingConditions()
    pOpc.print()
    
    pKuli = KulikovskyParameters()
    pKuli.print()
    
    Kuli = Kulikovsky()
    Kuli.print_diagnostics()
    
    
    
if __name__    =="__main__":
    parser=main()  