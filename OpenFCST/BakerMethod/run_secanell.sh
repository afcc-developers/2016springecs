#!/bin/bash
#############################################
#
# Bash script to run curves for all O2 conditions for all
# layers
#
#
#
##############################################

##################
#
# Function declarations
#
executing_simulations_function () {

dir=$1

#print variable on a screen
echo "Running tests in ${PWD} ..."
echo "Running tests in $dir ..."

echo "Running /1prc"
cd $1/1prc
rm *.vtu *.vtk polarization_curve.dat logfile.log
../../../bin/fuel_cell-2d.bin main.prm

echo "Running /1p5prc"
cd ../1p5prc
rm *.vtu *.vtk polarization_curve.dat logfile.log
../../../bin/fuel_cell-2d.bin main.prm
#  
echo "Running /2prc"
cd ../2prc
rm *.vtu *.vtk polarization_curve.dat logfile.log
../../../bin/fuel_cell-2d.bin main.prm
 
echo "Running /2p5prc"
cd ../2p5prc
rm *.vtu *.vtk polarization_curve.dat logfile.log
../../../bin/fuel_cell-2d.bin main.prm

cd ../

}

copying_function () {

dir=$1
CpDirectoryName=$2

echo "Copying files to $CpDirectoryName"

mkdir -p $CpDirectoryName
cp -r template $CpDirectoryName
cp -r $1/1prc $CpDirectoryName
cp -r $1/1p5prc $CpDirectoryName
cp -r $1/2prc $CpDirectoryName
cp -r $1/2p5prc $CpDirectoryName

}

clean_all () {

dir=$1

#print variable on a screen
echo "Cleaning tests in $dir ..."

cd $1/1prc
rm *.vtu *.vtk polarization_curve.dat logfile.log

cd ../1p5prc
rm *.vtu *.vtk polarization_curve.dat logfile.log
#  
cd ../2prc
rm *.vtu *.vtk polarization_curve.dat logfile.log
#
cd ../2p5prc
rm *.vtu *.vtk polarization_curve.dat logfile.log

cd ../
}
####################

####################
####################
# MAIN
####################
####################
echo "Script executed from: ${PWD}"

# #Read in directory where files will be stored:
# echo "Please, enter copy directory name relative to ${PWD}/archived_results"
# echo "For example, ICCP_k0p03_GDLporosity_85"
# read in
#in="GDLporosity72_ICCPboth_k0p005_DT"
in="2p1bar_358K_70RH"
CopyDirectoryName=${PWD}"/archived_results/"$in
echo "Directory: "$CopyDirectoryName
echo "Running..."

#
ExecutionName=${PWD}
executing_simulations_function $ExecutionName
copying_function $ExecutionName $CopyDirectoryName 
clean_all $ExecutionName $CopyDirectoryName

####################