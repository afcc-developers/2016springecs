# -*- coding: utf-8 -*-
"""
Auxiliary routines used to read the numerical data files in polarization_curve.dat.

Data is expected in the format:
Cell voltage [V]	Cathode current [A/cm2]	max_temperature	anode_current	water_cathode	water_anode	  proton_ohmic_heat	electron_ohmic_heat

@author: secanell
"""
from pylab import *
import csv
#import sys
import numpy as np
#import shutil

def ignore(row):
  "Returns true if line should be ignored"
  answer = False
  
  if len(row) == 0:
    "Line is just white space or has no delimiters"
    answer = True
  else:
     firstChars = row[0].strip()
     if len(firstChars) != 0:
         if firstChars[0] == '#':
            "Line's first non whitespace character is a '#'"
            answer = True     
    
  return answer


def readRow(row, cnvrt2Float = False):
  "Read currect csv row, stripping white space and converting to float if necessary"
  data = []
  
  for el in row:
    el = el.strip()
    
    if cnvrt2Float:
        try:
            data.append(float(el))
        except ValueError as e:
            print "Error converting '" + el + "' to a floating point number"
            raise ValueError
      
    else:
        data.append(el)
      
      
  return data

def readData(fileName, delim = '\t'):
    "Read data from file, returns metaData and data"
    metaData = []
    data     = []
   
    reader = csv.reader(open(fileName), delimiter = delim)
    
    readMetaData = False
    lineNo = 1
    for row in reader: 
        #Check to see if row should be ignored (if it is a comment)
        if not ignore(row):
            if readMetaData is False:
                rowData = readRow(row)
                metaData =  rowData
                readMetaData = True
            else:
                try:
                    rowData = readRow(row, True)
                    data.append(rowData)
                except ValueError:
                    print "Error reading data row ", row , "at line number", lineNo, "of file", fileName
                    raise ValueError
            
        lineNo +=1
	   
        
    return metaData,data
""" 
0- Cell voltage [V]	
1- Cathode current [A/cm2]	
2- max_temperature	
3- max_RH	
4- cathode_current	
5- water_cathode	
6- PEM_proton_ohmic_heat [W/cm2]
7- electron_ohmic_heat	[W/cm2] 
8- proton_ohmic_heat [W/cm2]
"""
def createData (Filename):
    
    meta0, data0 = readData(Filename)
    
    size_ = len(data0)
    voltage = [0]*size_
    current = [0]*size_
    RH_max = [0]*size_    
    water_flux = [0]*size_
    R_value = [0]*size_
    voltage_corrected = [0]*size_
    
    for i in range(size_):       
        voltage[i] = data0[i][0]
        current[i] = 1e3*data0[i][1]
        RH_max[i] = data0[i][3]
        water_flux[i] = data0[i][6]
        aux = data0[i][6]+data0[i][7]
        R_value[i] = aux/(data0[i][1]*data0[i][1]+1e-28)
        voltage_corrected[i] = voltage[i] + 1e-6*R_value[i]*current[i]
       
    return current, voltage, RH_max, water_flux, R_value, voltage_corrected