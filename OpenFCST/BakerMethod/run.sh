##!/bin/bash

######################################################################
# Create and run testcases 
# Sources:
#   - http://stackoverflow.com/questions/16483119/example-of-how-to-use-getopts-in-bash
#   - http://stackoverflow.com/questions/356100/how-to-wait-in-bash-for-several-subprocesses-to-finish-and-return-exit-code-0
######################################################################

usage() { echo "Usage: $0 [-p <int>]" 1>&2; exit 1; }

while getopts ":p:" o; do
    case "${o}" in
        p)
            num_proc=${OPTARG}
            ;;
        *)
            usage
            ;;
    esac
done
shift $((OPTIND-1))

if [ -z "${num_proc}" ]; then
    num_proc=1
fi

echo "\n\n\n\n"
echo "======================================================"
echo "= Run a non Dakota pressure variation sweep          ="
echo "======================================================"
echo "\n"


echo $(date)
echo "Execute ${num_proc} processes in parallel"

master_dir=$(pwd)
# echo $master_dir

dirs="\
'01ICCP_Wang/xO2_Air' \
'01ICCP_Wang/xO2_10p0prc' \
'01ICCP_Wang/xO2_5p0prc' \
'01ICCP_Wang/xO2_2p5prc' \
'01ICCP_Wang/xO2_2p0prc' \
'01ICCP_Wang/xO2_1p5prc' \
'01ICCP_Wang/xO2_1p0prc' \
'02ICCP_BV/xO2_Air' \
'02ICCP_BV/xO2_10p0prc' \
'02ICCP_BV/xO2_5p0prc' \
'02ICCP_BV/xO2_2p5prc' \
'02ICCP_BV/xO2_2p0prc' \
'02ICCP_BV/xO2_1p5prc' \
'02ICCP_BV/xO2_1p0prc' \
'05MH_Wang/xO2_Air' \
'05MH_Wang/xO2_10p0prc' \
'05MH_Wang/xO2_5p0prc' \
'05MH_Wang/xO2_2p5prc' \
'05MH_Wang/xO2_2p0prc' \
'05MH_Wang/xO2_1p5prc' \
'05MH_Wang/xO2_1p0prc' \
'06MH_BV/xO2_Air' \
'06MH_BV/xO2_10p0prc' \
'06MH_BV/xO2_5p0prc' \
'06MH_BV/xO2_2p5prc' \
'06MH_BV/xO2_2p0prc' \
'06MH_BV/xO2_1p5prc' \
'06MH_BV/xO2_1p0prc' \
"




for T in $dirs ; do echo "$T" ; done | 
  (
    export master_dir
    xargs -I{} --max-procs $num_proc bash -c '
    {
      
      cd $master_dir
      i={}
      directory=$i
      
      echo "Execute run, working directory: $directory, logfile: fcst.log"
      cd ${directory}
      
      rm *.vtu *.vtk *.dat *.log &> /dev/null
      ls
      
      mpirun -np 2 fuel_cell-2d.bin main.prm &> fcst.log
      
      fcst_create_pvd.py >> fcst.log
      
      echo "Completed run in directory $directory"
      
    }'
  )

echo $(date)