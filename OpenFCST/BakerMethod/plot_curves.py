# -*- coding: utf-8 -*-
"""
Created on Wed Jul 22 15:24:14 2015

@author: secanell
"""

import read_data as Data
from pylab import plt
from scipy import optimize
from scipy import *


def R_MT(ilim, co2):
    F = 96485
    R = co2*4*F/ilim
    
    return R
    
"""
 Main function for the Python script:
 
"""
if __name__ == "__main__":
    
    root_path ='./archived_results/2p6bar_358K_70RH/'    
    path=root_path+'1prc/polarization_curve.dat'
    
    pressure = 260000 #Pa
    prco2 = [0.01, 0.015, 0.02, 0.025]      #
    F = 96485
    T = 358.15        # K
    R = 8.314;
    T_celsius = T - 273.15;
    p_sat = 101325*pow(10,(-2.1794+0.02953*T_celsius-0.000091837*pow(T_celsius,2)+0.00000014454*pow(T_celsius,3)));
  
    c_o2_wet = [0]*len(prco2)
    c_o2 = [0]*len(prco2)
    for i in range(len(prco2)):
        c_o2_wet[i] = prco2[i]*(pressure - p_sat)/(R*T)
        c_o2[i] = prco2[i]*pressure/(R*T)
    
    styles = ['--ks','--b^','--ro','--g*','--m>']
    
    Rtot = [0,0,0,0]
    ilim = [0,0,0,0]
    
    current, voltage, RH_max, water_flux, R_value, voltage_corrected = Data.createData(path) 
    plt.figure(1)
    plt.plot(current, voltage, styles[0],label='1prc - '+str(pressure/1000)+'kPa')
    plt.figure(2)
    plt.plot(current, R_value, styles[0],label='1prc - '+str(pressure/1000)+'kPa')
    Rtot[0] = R_MT(current[-1], c_o2[0])
    ilim[0] = current[-1]*1e-3*1e4  #mA -> A and 1/cm2 to 1/m2
    
    path=root_path+'1p5prc/polarization_curve.dat'
    current, voltage, RH_max, water_flux, R_value, voltage_corrected = Data.createData(path) 
    plt.figure(1)
    plt.plot(current, voltage, styles[1],label='1p5prc - '+str(pressure/1000)+'kPa')
    plt.figure(2)
    plt.plot(current, R_value, styles[1],label='1p5prc - '+str(pressure/1000)+'kPa')   
    Rtot[1] = R_MT(current[-1], c_o2[1])
    ilim[1] = current[-1]*1e-3*1e4
    
    path=root_path+'2prc/polarization_curve.dat'
    current, voltage, RH_max, water_flux, R_value, voltage_corrected = Data.createData(path) 
    plt.figure(1)
    plt.plot(current, voltage, styles[2],label='2prc - '+str(pressure/1000)+'kPa')
    plt.figure(2)
    plt.plot(current, R_value, styles[2],label='2prc - '+str(pressure/1000)+'kPa')
    Rtot[2] = R_MT(current[-1], c_o2[2])
    ilim[2] = current[-1]*1e-3*1e4

    path=root_path+'2p5prc/polarization_curve.dat'
    current, voltage, RH_max, water_flux, R_value, voltage_corrected = Data.createData(path) 
    plt.figure(1)
    plt.plot(current, voltage, styles[3],label='2p5prc - '+str(pressure/1000)+'kPa')
    plt.figure(2)
    plt.plot(current, R_value, styles[3],label='2p5prc - '+str(pressure/1000)+'kPa')  
    Rtot[3] = R_MT(current[-1], c_o2[3])
    ilim[3] = current[-1]*1e-3*1e4
    
    # Plot nicely:    
    plt.figure(1)
    plt.xlabel('Current Density (mA/cm^2)', fontsize=16)
    plt.ylabel('Cell Voltage (V)', fontsize=16)
    plt.title('Polarization curves using 1%, 1.5%,, 2% and 2.5% O2', fontsize=16)
    plt.grid('on')    
    plt.legend(loc=1,prop={'size':14})       #loc=1 top-right, loc=3 bottom-left
    
    plt.figure(2)    
    plt.xlabel('Current Density (mA/cm^2)', fontsize=16)
    plt.ylabel('Cell resistance (Ohms cm^2)', fontsize=16)
    plt.title('Cell resistance using 1%, 1.5%,, 2% and 2.5% O2', fontsize=16)
    plt.grid('on')
    plt.legend(loc=1,prop={'size':14})       #loc=1 top-right, loc=3 bottom-left
    
    plt.figure(3)
    plt.plot(c_o2_wet, ilim, 'b^')
    plt.grid('on')
    plt.ylabel('Limiting Current Density (A/m^2)', fontsize=16)
    plt.xlabel('Oxygen concentration (wet) (mol/m^3)', fontsize=16)
    
    # Fit the first set
    fitfunc = lambda p, x: p*x # Target function
    errfunc = lambda p, x, y: fitfunc(p, x) - y # Distance to the target function
    p0 = 1 # Initial guess for the parameters
    p1, success = optimize.leastsq(errfunc, p0, args=(c_o2_wet, ilim))
    time = linspace(0.0, 2.0, 100)
    R_t = 4*F/p1
    text='R_T='+array_str(R_t)+' at '+str(pressure/1000)+' kPa'
    plt.plot(time, fitfunc(p1, time), "b--",label=text) # Plot of the data and the fit
    plt.legend(loc=1,prop={'size':14})       #loc=1 top-right, loc=3 bottom-left
    